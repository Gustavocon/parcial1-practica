package biblioteca;

import biblioteca.*;
public class Biblioteca {
    public static void main(String[] args) {
        Articulo libro = new Libro(33, "Las aventuras del programador que recursaba", 2021, true);
        Articulo revista = new Revista(20,"Las aventuras del programador/a que leyo esto y recurso tambien",2021,2);

        // Leer informacion del libro
        System.out.println("Codigo: " + libro.getCodigo());
        System.out.println("Titulo: " + libro.getTitulo());
        System.out.println("Año de publicacion: " + libro.getAñoPublicacion());
        System.out.print("El libro ");
        if (!libro.prestado())
        {
            System.out.print("no ");
        }
        System.out.println("fue prestado");

        // Leer informacion de la revista
        System.out.println("Codigo: " + revista.getCodigo());
        System.out.println("Titulo: " + revista.getTitulo());
        System.out.println("Año de publicacion: " + revista.getAñoPublicacion());
        System.out.println("Numero correlativo: " + revista.getNumeroCorrelativo());
    }
}
