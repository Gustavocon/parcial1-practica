package biblioteca;
public class Libro extends Articulo implements Prestable{
    boolean prestado;

    public Libro(int codigo, String titulo, int anoPublicacion, boolean prestado) {
        super(codigo, titulo, anoPublicacion);
        this.prestado = prestado;
    }


    @Override
    public void devolver() {
        this.prestado = false;
    }

    @Override
    public void prestar() {
        this.prestado = true;
    }

    @Override
    public boolean prestado() {
        return this.prestado;
    }



    
}
